
const digger = require('../digger.js');

const cfg = require('../configs/config.json');

describe('digger', ()=>{
    beforeEach(() => {
        digger.reset();
    })

    it('should get all parts <...>', () => {
        let str = '';
        str += '<html data-path="/p/:id"><head>';
        str += '<meta charset="UTF-8">';
        str += '<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">';
        str += '<meta http-equiv="X-UA-Compatible" content="ie=edge">';
        str += '<meta name="renderer" content="webkit">';
        str += '<meta name="baidu-site-verification" content="ET7tYDCqIv">';
        str += '<meta name="apple-mobile-web-app-title" content="Title">';
        str += '<meta name="apple-mobile-web-app-capable" content="yes">';
        str += '<meta name="apple-mobile-web-app-status-bar-style" content="black">';

        digger.config(cfg.html, str);
        digger.dig();
        const tags = digger.tags();

        expect(tags[0]).toEqual('<html data-path="/p/:id">');
        expect(tags[1]).toEqual('<head>');
        expect(tags[2]).toEqual('<meta charset="UTF-8">');
        expect(tags[3]).toEqual('<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">');
        expect(tags[4]).toEqual('<meta http-equiv="X-UA-Compatible" content="ie=edge">');
    })

    it('should dig out all stuff matching the rules', () => {
        let str = '';
        str += '<meta name="format-detection" content="telephone=no"><meta data-react-helmet="true" property="og:title" content="">';
        str += '<meta data-react-helmet="true" property="og:description" content=""><meta data-react-helmet="true" property="og:image" content="">';
        str += '<meta data-react-helmet="true" property="og:type" content="article"><meta data-react-helmet="true" name="description" content="">';
        str += '<title></title>';
        str += '<link href="//sta.36krcnd.com/36krx2018-front/static/app.4d5c3d3c.css" rel="stylesheet">';
        str += '<link rel="stylesheet" type="text/css" href="//sta.36krcnd.com/36krx2018-front/static/article.a412b219.css">';
        str += '<link href="//36kr.com/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon">';

        digger.config(cfg.html, str);
        digger.dig();
        const originalAddrList = digger.originalAddrList();

        expect(originalAddrList[0]).toEqual('//sta.36krcnd.com/36krx2018-front/static/app.4d5c3d3c.css');
        expect(originalAddrList[1]).toEqual('//sta.36krcnd.com/36krx2018-front/static/article.a412b219.css');
        expect(originalAddrList[2]).toEqual('//36kr.com/favicon.ico');
    })

    it('should dig out all stuff matching the rules and ignore all other ones', () => {
        let str = '';
        str += '<div class="article-footer-txt"><p></p></div></div></div></div></div><div class="kr-article-company"><div></div></div>';
        str += '<div class="ad87-box"></div><div class="thumb-up-box"><div class="thumb-inner"><div class="thumb-wrapper"><div class="backgroundImg-box-wrapper">';
        str += '<div class="backgroundImg-box showNormal"><span class="kr-article-tranistion-thumb  normalColor">+1</span><div class="thumbNum normalColor">38</div></div></div>';
        str += '<img class="author-avatar-img" src="https://pic.36krcnd.com/avatar/201702/10070449/32voo01q63ac8uxx.jpg!120" alt=""></a>';
        str += '<div class="item-left"><span class="item-avatar" style="background-image: url(&quot;http://wx.qlogo.cn/mmopen/vi_32/11/0&quot;);"></span></div>';
        str += '<img class="weixin-code" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIcAAACHCAIAAACzhd1dAAAABnRSTlMA/wD/A/3/Z/6/y/s/F/t/3/Q/dggg==" alt="">';
        str += '<i class="weixin-txt"></i></span></span><a class="item-share-weibo item-common" href="http://share.baidu.com/s?type=text&amp;searchPic=1&amp;sign=on">';
        str += '<link href="//sta.36krcnd.com/36krx2018-front/static/app.4d5c3d3c.xss" rel="stylesheet">';
        
        digger.config(cfg.html, str);
        digger.dig();
        const originalAddrList = digger.originalAddrList();

        expect(originalAddrList[0]).toEqual('https://pic.36krcnd.com/avatar/201702/10070449/32voo01q63ac8uxx.jpg!120');
        expect(originalAddrList[1]).toEqual('&quot;http://wx.qlogo.cn/mmopen/vi_32/11/0&quot;');
        expect(originalAddrList[2]).toEqual(undefined);
        expect(originalAddrList[3]).toEqual(undefined);
        expect(originalAddrList[4]).toEqual(undefined);
    })
})