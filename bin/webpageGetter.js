#!/usr/bin/env node

const log = require('console').log;
const readlineSync = require('readline-sync');
const clipboard = require('clipboardy');
const WebpageGetter = require('../src/webpageGetter');

readlineSync.question("Copy url and press any key!");
const url = clipboard.readSync();
readlineSync.question("Copy html content and press any key!");
let content = clipboard.readSync();

let webpageGetter = new WebpageGetter({ output: "output" });

const dest = webpageGetter.genIdentifier();

webpageGetter.saveAs(url, content, dest).then(() => {
    log(dest, " save done.");
})