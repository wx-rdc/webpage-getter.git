# WebpageGetter

Nodejs版本的网页另存功能。

## 调用示例

```
const { WebpageGetter } = require('webpage-getter');

let webpageGetter = new WebpageGetter();

// url: https://www.whatyouwant.com/

// content: 网页渲染后的内容，通常可以使用无头浏览器或者[splash]()来自动获取

// 你可以使用其它方式生成目标名
const dest = webpageGetter.genIdentifier();

webpageGetter.saveAs(url, content, dest).then(() => {
    log(dest, " save done.");
})
```

## 致谢

本项目来源于 `Raymond He` 的 `webpageGetter` 项目，由 `Edward Zhu` 进行了改写，修复了若干Bug，并生成NPM包。

## 命令行使用帮助

本项目仍然保留了命令行的使用方式，你可以执行如下命令启动命令行工具
```
webpageGetter
```

下面是原作者的使用说明：

这个小工具被设计用来下载你正在浏览的网页及其关联的资源文件。
所谓关联的资源文件包括图像，样式文件，脚本文件等等。
针对样式文件，其所引用的图像文件也会被保存下来。
使用这个工具需要提供实际的网页内容而不是最初由服务器返回的HTML。
这是因为一些网页只有当你滚动到页面最下端或者等待脚本文件执行完毕，才会真正渲染完成。

保存网页前，需要你提供：
1. 网址，例如 "https://www.whatyouwant.com/"
2. 实际的网页内容。
拿Chrome举例， 你可以这样获得实际的网页内容：
打开开发者工具， 切换到Elements标签， 右击 html 元素， 出现上下文菜单， 选择 Copy > Copy outerHTML。
最终， 你会得到一个HTML文件 - YYMMDDHHMMSS.html 和一个资源文件夹 - YYMMDDHHMMSS。
希望这个工具能对大家有点儿小用！
        -- "Raymond He"