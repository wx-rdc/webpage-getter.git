
const path = require('path');

const parser = require('../cssParser.js');

const cfg = require('../configs/config.json');

const file = '1_mock.css';
const dest = '1122334455';

describe('parser', ()=>{
    beforeEach(() => {
        parser.reset();
        parser.mockId('mId');
    })

    it('should handle the ones which needs to be merged with hostname', () => {
        const urls = new Array();
        urls.push('images/body_bg.jpg');
        urls.push('\'images/icoLouZhu.gif\'');
        urls.push('\'images/comment.gif\'');

        parser.config(cfg.css, dest, file, [{css: '', dir: ''}], urls);
        parser.mockCssBox(file, [{css: '1_mock.css', dir: 'https://www.YouSeeYouGet.com/skins/blacklowkey/'}]);
        parser.parse();
        const downloadAddrList = parser.downloadAddrList();
        const saveAddrList = parser.saveAddrList();

        expect(downloadAddrList[0]).toEqual('https://www.YouSeeYouGet.com/skins/blacklowkey/images/body_bg.jpg');
        expect(saveAddrList[0]).toEqual(path.join(dest, 'css_mId_body_bg.jpg'));
        expect(downloadAddrList[1]).toEqual('https://www.YouSeeYouGet.com/skins/blacklowkey/images/icoLouZhu.gif');
        expect(saveAddrList[1]).toEqual(path.join(dest, 'css_mId_icoLouZhu.gif'));
        expect(downloadAddrList[2]).toEqual('https://www.YouSeeYouGet.com/skins/blacklowkey/images/comment.gif');
        expect(saveAddrList[2]).toEqual(path.join(dest, 'css_mId_comment.gif'));
    })
})