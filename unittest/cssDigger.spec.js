
const digger = require('../cssDigger.js');

const cfg = require('../configs/config.json');

describe('cssDigger', ()=>{
    beforeEach(() => {
        digger.reset();
    })

    it('should dig out all stuff matching the css regexp', () => {
        let str = '';
        str += 'body {';
        str += '	color: #000';
        str += '	background: #fff';
        str += '	font-family: Verdana,Arial,Helvetica,sans-serif';
        str += '	font-size: 12px';
        str += '	min-height: 101%';
        str += '	background: url(images/body_bg.jpg) top center no-repeat #000';
        str += '}';
        str += '.louzhu {';
        str += '	background: transparent url(\'images/icoLouZhu.gif\') no-repeat scroll right top';
        str += '	padding-right: 16px';
        str += '}';
        str += '.feedbackCon {';
        str += '	border-bottom: 1px solid #ccc';
        str += '	background: url(\'images/comment.gif\') no-repeat 5px 0';
        str += '.postBody blockquote {';
        str += '	background: url(\'images/comment.gif\')no-repeat 25px 0';

        digger.config(cfg.css, str);
        digger.dig();
        const originalAddrList = digger.originalAddrList();

        expect(originalAddrList[0]).toEqual('images/body_bg.jpg');
        expect(originalAddrList[1]).toEqual('\'images/icoLouZhu.gif\'');
        expect(originalAddrList[2]).toEqual('\'images/comment.gif\'');
        expect(originalAddrList[3]).toEqual(undefined);
    })
})