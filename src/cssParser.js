'use strict'

const path = require('path');
const url = require('url');

class cssParser {
    constructor(options) {
        this._cfg = options;
        this._dest = '';
        this._dir = '';
        this._originalAddrList = new Array();
        this._mId = '';
        this._prefixNum = 0;
        this._downloadAddrList = new Array();
        this._saveAddrList = new Array();
        this._replaceAddrList = new Array();
    }

    downloadAddrList() { return this._downloadAddrList; }

    saveAddrList() { return this._saveAddrList; }

    replaceAddrList() { return this._replaceAddrList; }


    parse(saveTo, file, box, arrUrls) {
        this._dest = saveTo;

        box.forEach((item) => {
            if (item.css === file) {
                this._dir = item.dir;
            }
        });

        this._originalAddrList = arrUrls;

        const digFileName = (url) => {
            let arr = url.split('/');
            let fileName = arr.length > 1 ? arr[arr.length - 1] : url;

            let exclamationPos = fileName.indexOf('!');
            let questionPos = fileName.indexOf('?');
            if (exclamationPos > -1 || questionPos > -1) {
                if (exclamationPos > -1) fileName = fileName.substr(0, exclamationPos);
                if (questionPos > -1) fileName = fileName.substr(0, questionPos);
            }

            this._prefixNum += 1;

            return path.join(this._dest, 'css_' + this._prefixNum.toString(16) + '_' + fileName);
        }

        const parseUrl = (item) => {
            const parsed = digFileName(item);
            const firstOne = item.substr(0, 1);
            const firstTwo = item.substr(0, 2);
            const urlParsed = url.parse(this._dir);
            // const protocol = dir.substr(0, 7) === 'http://' ? 'http://' : 'https://';
            const protocl = urlParsed.protocol + '//';
            const hostname = urlParsed.host;

            if (item.substr(0, 7) === 'http://' || item.substr(0, 8) === 'https://') {
                return {
                    downloadAddr: item,
                    saveAddr: parsed,
                    replaceAddr: parsed.substr(parsed.indexOf('\\') + 1)
                };
            }

            if (firstTwo === '//') {
                return {
                    downloadAddr: protocol + item.substr(2),
                    saveAddr: parsed,
                    replaceAddr: parsed.substr(parsed.indexOf('\\') + 1)
                };
            }

            if (firstOne === '/') {
                return {
                    downloadAddr: protocl + hostname + item,
                    saveAddr: parsed,
                    replaceAddr: parsed.substr(parsed.indexOf('\\') + 1)
                };
            } else {
                return {
                    downloadAddr: dir + item,
                    saveAddr: parsed,
                    replaceAddr: parsed.substr(parsed.indexOf('\\') + 1)
                };
            }
        }

        this._originalAddrList.forEach((addr) => {
            let surround = '';
            let matched = false;

            this._cfg.surround.forEach((sur) => {
                if (!matched && addr.substr(0, sur.length) === sur) {
                    matched = true;
                    surround = sur;
                    addr = addr.substr(surround.length, addr.length - 2 * surround.length);
                }
            });

            const ret = parseUrl(addr);
            this._downloadAddrList.push(ret.downloadAddr);
            this._saveAddrList.push(ret.saveAddr);
            surround != '' ? this._replaceAddrList.push(surround + ret.replaceAddr + surround) : this._replaceAddrList.push(ret.replaceAddr);
        });
    }
}

module.exports = cssParser;

/*
module.exports = {
    reset: reset,
    config: config,
    mockId: mockId,
    mockCssBox: mockCssBox,
    parse: parse,
    downloadAddrList: () => { return downloadAddrList; },
    saveAddrList: () => { return saveAddrList; },
    replaceAddrList: () => { return replaceAddrList; }
};
*/