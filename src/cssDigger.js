'use strict'

class CssDigger {

    constructor(options) {
        this._cfg = options;
        this._originalAddrList = new Array();
    }

    originalAddrList() { return this._originalAddrList; }

    dig(content) {
        let arrImgs = [];

        this._cfg.regs.forEach(reg => {
            arrImgs = [...arrImgs, ...content.matchAll(new RegExp(reg, 'g'))]
        })
        arrImgs.forEach((item) => {
            const val = item[1];
            let check = true;

            this._cfg.ignoreProp.forEach((item) => {
                if (check && val.match(new RegExp(item))) {
                    check = false;
                }
            });

            if (val.trim().length > 0 && check && this._originalAddrList.indexOf(val) === -1) {
                this._originalAddrList.push(val);
            }
        });
    }
}

module.exports = CssDigger;

/*
module.exports = {
    reset: reset,
    config: config,
    dig: dig,
    originalAddrList: () => { return originalAddrList; }
}
*/
