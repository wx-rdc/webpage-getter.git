#!/usr/bin/env node

const log = require('console').log;
const warn = require('console').warn;
const readlineSync = require('readline-sync');
const clipboard = require('clipboardy');
const WebpageGetter = require('../src/webpageGetter');

readlineSync.question("Copy url and press any key!");
const url = clipboard.readSync();

let webpageGetter = new WebpageGetter({ output: "output", proxy: "http://localhost:8123", splashServer: "http://localhost:8050" });

webpageGetter.fetch(url).then((dest) => {
    log(dest, " save done.");
}).catch(error => {
    warn(error);
})
