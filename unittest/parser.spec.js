
const path = require('path');

const parser = require('../parser.js');

const cfg = require('../configs/config.json');

const req = 'https://www.YouSeeYouGet.com';
const dest = '1122334455';

describe('parser', () => {
    beforeEach(() => {
        parser.reset();
        parser.mockId('mId');
    })

    it('should "config" a url with http:// or https://', () => {
        expect(() => {
            parser.config(cfg.html, dest, '//www.xyz.com', new Array());
        }).toThrow();
    })

    it('should add / in the end of url if needed', () => {
        parser.config(cfg.html, dest, req, new Array());

        expect(parser.currentURL()).toEqual('https://www.YouSeeYouGet.com/');
    })

    it('should handle the ones which begins with // http:// https://', () => {
        const urls = new Array();
        urls.push('//sta.36krcnd.com/36krx2018-front/static/app.4d5c3d3c.css');
        urls.push('//sta.36krcnd.com/36krx2018-front/static/article.a412b219.css');
        urls.push('//36kr.com/favicon.ico');
        urls.push('https://pic.36krcnd.com/avatar/201702/10070449/32voo01q63ac8uxx.jpg!120');

        parser.config(cfg.html, dest, req, urls);
        parser.parse();
        const downloadAddrList = parser.downloadAddrList();
        const saveAddrList = parser.saveAddrList();

        expect(downloadAddrList[0]).toEqual('https://sta.36krcnd.com/36krx2018-front/static/app.4d5c3d3c.css');
        expect(saveAddrList[0]).toEqual(path.join(dest, 'mId_app.4d5c3d3c.css'));
        expect(downloadAddrList[1]).toEqual('https://sta.36krcnd.com/36krx2018-front/static/article.a412b219.css');
        expect(saveAddrList[1]).toEqual(path.join(dest, 'mId_article.a412b219.css'));
        expect(downloadAddrList[2]).toEqual('https://36kr.com/favicon.ico');
        expect(saveAddrList[2]).toEqual(path.join(dest, 'mId_favicon.ico'));
        expect(downloadAddrList[3]).toEqual('https://pic.36krcnd.com/avatar/201702/10070449/32voo01q63ac8uxx.jpg!120');
        expect(saveAddrList[3]).toEqual(path.join(dest, 'mId_32voo01q63ac8uxx.jpg'));
    })

    it('should handle the ones which ends with ! ? or other shit', () => {
        const urls = new Array();
        urls.push('https://pic.36krcnd.com/avatar/201702/10070449/32voo01q63ac8uxx.jpg!120');

        parser.config(cfg.html, dest, req, urls);
        parser.parse();
        const downloadAddrList = parser.downloadAddrList();
        const saveAddrList = parser.saveAddrList();

        expect(downloadAddrList[0]).toEqual('https://pic.36krcnd.com/avatar/201702/10070449/32voo01q63ac8uxx.jpg!120');
        expect(saveAddrList[0]).toEqual(path.join(dest, 'mId_32voo01q63ac8uxx.jpg'));
    })

    it('should handle the ones which be surround with &quot;', () => {
        const urls = new Array();
        urls.push('&quot;http://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJbuxpls8cAmkz39KdxnguJDSQtCI84u0nAlfGSFmfjDn25YMkRic9ibG0THWCMb0yTDxdGSECLQmdg/0&quot;');

        parser.config(cfg.html, dest, req, urls);
        parser.parse();
        const downloadAddrList = parser.downloadAddrList();
        const saveAddrList = parser.saveAddrList();

        expect(downloadAddrList[0]).toEqual('http://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJbuxpls8cAmkz39KdxnguJDSQtCI84u0nAlfGSFmfjDn25YMkRic9ibG0THWCMb0yTDxdGSECLQmdg/0');
        expect(saveAddrList[0]).toEqual(path.join(dest, 'mId_0'));
    })

    it('should handle the ones which needs to be merged with hostname', () => {
        const urls = new Array();
        urls.push('/static/app.4d5c3d3c.css');
        urls.push('favicon.ico');

        parser.config(cfg.html, dest, req, urls);
        parser.parse();
        const downloadAddrList = parser.downloadAddrList();
        const saveAddrList = parser.saveAddrList();

        expect(downloadAddrList[0]).toEqual('https://www.YouSeeYouGet.com/static/app.4d5c3d3c.css');
        expect(saveAddrList[0]).toEqual(path.join(dest, 'mId_app.4d5c3d3c.css'));
        expect(downloadAddrList[1]).toEqual('https://www.YouSeeYouGet.com/favicon.ico');
        expect(saveAddrList[1]).toEqual(path.join(dest, 'mId_favicon.ico'));
    })
})
